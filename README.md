

## How it looks
https://i.imgur.com/aPCyBeo.jpg

## Example usage

```
import React, { Component } from 'react';
import Pagination from 'office-ui-fabric-react-pagination';

class App extends Component {
  render() {
    return (
		<Pagination
            currentPage={1}
            totalPages={10}
            onChange={(page) => {alert('Send user to page: ' + page)}}
		/>
    );
  }
}

export default App;
```

## Example usage with fabric icons

If you're missing the chevron icons you need to install the '@uifabric/icons' npm package, and call initializeIcons().
Eg.:

```
import React, { Component } from 'react';
import { initializeIcons } from '@uifabric/icons';
import Pagination from 'office-ui-fabric-react-pagination';
initializeIcons();

class App extends Component {
  render() {
    return (
		<Pagination
            currentPage={1}
            totalPages={10}
            onChange={(page) => {alert('Send user to page: ' + page)}}
		/>
    );
  }
}

export default App;
```

Pagination component has the following interface:
(copied from [react-ultimate-pagination](https://www.npmjs.com/package/react-ultimate-pagination))

* __currentPage__: number - current page number
* __totalPages__: number - total number of pages
* __boundaryPagesRange__: number, optional, default: 1 - number of always visible pages at the beginning and end
* __siblingPagesRange__: number, optional, default: 1 - number of always visible pages before and after the current one
* __hideEllipsis__: bool, optional, default: false - boolean flag to hide ellipsis
* __hidePreviousAndNextPageLinks__: bool, optional, default: false - boolean flag to hide previous and next page links
* __hideFirstAndLastPageLinks__: bool, optional, default: false - boolean flag to hide first and last page links
* __onChange__: function - callback that will be called with new page when it should be changed by user interaction (optional)