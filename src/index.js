import React from 'react';
import * as ReactUltimatePagination from 'react-ultimate-pagination';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';

function Page(props) {
    return <DefaultButton primary={props.isActive} onClick={props.onClick} text={props.value} />;
}

function Ellipsis(props) {
    return <DefaultButton onClick={props.onClick} text='...' />;
}

function FirstPageLink(props) {
    return <DefaultButton onClick={props.onClick} iconProps={{ iconName: 'DoubleChevronLeft' }} />;
}

function PreviousPageLink(props) {
    return <DefaultButton onClick={props.onClick} iconProps={{ iconName: 'ChevronLeft' }} />;
}

function NextPageLink(props) {
    return <DefaultButton onClick={props.onClick} iconProps={{ iconName: 'ChevronRight' }} />;
}

function LastPageLink(props) {
    return <DefaultButton onClick={props.onClick} iconProps={{ iconName: 'DoubleChevronRight' }} />;
}

var itemTypeToComponent = {
    'PAGE': Page,
    'ELLIPSIS': Ellipsis,
    'FIRST_PAGE_LINK': FirstPageLink,
    'PREVIOUS_PAGE_LINK': PreviousPageLink,
    'NEXT_PAGE_LINK': NextPageLink,
    'LAST_PAGE_LINK': LastPageLink
};

class Pagination extends React.Component {

    render(){
        const parentProps = this.props;
        const CreatedPagination = parentProps.wrapperStyle ? 
        ReactUltimatePagination.createUltimatePagination({
            itemTypeToComponent: itemTypeToComponent,
            WrapperComponent: (props) => <div style={parentProps.wrapperStyle} className='pagination'>{props.children}</div>
        })
        :
        ReactUltimatePagination.createUltimatePagination({
            itemTypeToComponent: itemTypeToComponent,
            WrapperComponent: (props) => <div className='pagination'>{props.children}</div>
        });
        return (<CreatedPagination {...parentProps}/>);
    }

}

export default Pagination;